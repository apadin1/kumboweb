<?php

namespace app\controllers;

use yii\web\Controller;

class MesesController extends Controller
{
    public function actionMiVista()
    {
        return $this->render('meses');
    }
}

