<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Ingresos $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="ingresos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'saldo')->textInput() ?>

    <?= $form->field($model, 'fecha')->textInput() ?>

    <?= $form->field($model, 'concepto')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'emisor')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'idobjetivofinanciero')->textInput() ?>

    <?= $form->field($model, 'idventa')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
