<?php

/** @var yii\web\View $this */

$this->title = 'My Yii Application';
?>
<body>
 
  <section class="hero">
    <div class="container">
      <h2>Meses</h2>
      <p>Estamos aquí para ayudarte a alcanzar tus metas financieras. Con nuestras herramientas intuitivas y orientación experta, podrás ahorrar y hacer crecer tu capital de manera fácil y segura.

Únete a nosotros y comencemos juntos este emocionante viaje hacia un futuro financiero sólido. ¡Vamos a empezar a ahorrar y construir juntos!</p>
    
    </div>
  </section>

  <footer>
    <div class="container">
      <p>Derechos de autor &copy; 2024 Nuestra Empresa.</p>
    </div>
  </footer>
</body>