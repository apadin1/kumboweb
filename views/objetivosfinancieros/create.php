<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Objetivosfinancieros $model */

$this->title = 'Create Objetivosfinancieros';
$this->params['breadcrumbs'][] = ['label' => 'Objetivosfinancieros', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="objetivosfinancieros-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
