<?php

/** @var yii\web\View $this */

use yii\helpers\Html;

$this->title = 'My Yii Application';
?>
<!DOCTYPE html>
<html lang="es">


<body>
  <div class="contenido-principal">
    <!-- IMAGEN DE FONDO -->
    <div class="fondo-pantalla">
      <?= Html::img(
        '@web/imagenes/edificio.jpg',
        [
          'alt' => 'Playa - Olas',
          'class' => 'img-fluid'
        ]
      );
      ?>
    </div>

    <div class="info-principal hero">
      <h2>Descubre Kumbo</h2>
      <p class="texto-principal">Estamos aquí para ayudarte a alcanzar tus metas financieras. Con nuestras herramientas intuitivas y orientación experta, podrás ahorrar y hacer crecer tu capital de manera fácil y segura.

      Únete a nosotros y comencemos juntos este emocionante viaje hacia un futuro financiero sólido. ¡Vamos a empezar a ahorrar y construir juntos!</p>

    </div>
  </div>



</body>

</html>