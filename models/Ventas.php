<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ventas".
 *
 * @property int $id
 * @property int|null $saldo
 * @property string|null $fecha
 * @property string|null $concepto
 * @property string|null $plataforma
 * @property string|null $objeto
 *
 * @property Ingresos $ingresos
 */
class Ventas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ventas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['saldo'], 'integer'],
            [['fecha'], 'safe'],
            [['concepto'], 'string', 'max' => 200],
            [['plataforma'], 'string', 'max' => 100],
            [['objeto'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'saldo' => 'Saldo',
            'fecha' => 'Fecha',
            'concepto' => 'Concepto',
            'plataforma' => 'Plataforma',
            'objeto' => 'Objeto',
        ];
    }

    /**
     * Gets query for [[Ingresos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIngresos()
    {
        return $this->hasOne(Ingresos::class, ['idventa' => 'id']);
    }
}
