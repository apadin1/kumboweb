<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "compras".
 *
 * @property int $id
 * @property int|null $iva21
 * @property int|null $iva10
 * @property int|null $iva4
 * @property string|null $concepto
 * @property int|null $precio
 * @property string|null $fecha
 * @property string|null $proveedor
 *
 * @property Gastos $gastos
 */
class Compras extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'compras';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['iva21', 'iva10', 'iva4', 'precio'], 'integer'],
            [['fecha'], 'safe'],
            [['concepto'], 'string', 'max' => 200],
            [['proveedor'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'iva21' => 'Iva21',
            'iva10' => 'Iva10',
            'iva4' => 'Iva4',
            'concepto' => 'Concepto',
            'precio' => 'Precio',
            'fecha' => 'Fecha',
            'proveedor' => 'Proveedor',
        ];
    }

    /**
     * Gets query for [[Gastos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGastos()
    {
        return $this->hasOne(Gastos::class, ['idcompra' => 'id']);
    }
}
