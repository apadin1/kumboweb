<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "gastos".
 *
 * @property int $id
 * @property int|null $saldo
 * @property string|null $fecha
 * @property string|null $concepto
 * @property string|null $tipo
 * @property int|null $idobjetivofinanciero
 * @property int|null $idcompra
 *
 * @property Compras $idcompra0
 */
class Gastos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'gastos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['saldo', 'idobjetivofinanciero', 'idcompra'], 'integer'],
            [['fecha'], 'safe'],
            [['concepto'], 'string', 'max' => 200],
            [['tipo'], 'string', 'max' => 10],
            [['idcompra'], 'unique'],
            [['idcompra'], 'exist', 'skipOnError' => true, 'targetClass' => Compras::class, 'targetAttribute' => ['idcompra' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'saldo' => 'Saldo',
            'fecha' => 'Fecha',
            'concepto' => 'Concepto',
            'tipo' => 'Tipo',
            'idobjetivofinanciero' => 'Idobjetivofinanciero',
            'idcompra' => 'Idcompra',
        ];
    }

    /**
     * Gets query for [[Idcompra0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdcompra0()
    {
        return $this->hasOne(Compras::class, ['id' => 'idcompra']);
    }
}
